/*****************************************************************************
Arduino library handling ping messages for the esp8266 platform

MIT License

Copyright (c) 2018 Alessio Leoncini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

/****************************************************************************
 * Adafruit NeoPixel code theirs
 * 
 * Other bits Copyright Aaron S. Crandall <crandall@gonzaga.edu>, 2020 
 *   - MIT License too, why not? * 
 */

#include <Arduino.h>
#include <Pinger.h>
#include <ESP8266WiFi.h>
extern "C"
{
  #include <lwip/icmp.h> // needed for icmp packet definitions
}

#include <Adafruit_NeoPixel.h>
#define LED_PIN    D2
#define LED_COUNT 7
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// ** A single host to ping and track stats for - too big, but fine... *********************
class Host {
  IPAddress myIP = NULL;
  Pinger myPinger;
  Adafruit_NeoPixel * theStrip = NULL;
  String myHostname = "";

  public:
  int myID = 0;
  String myName = "unknown";
  int TotalSentRequests = 0;
  int TotalReceivedResponses = 0;
  float AvgResponseTime = 0.0;
  float PingLoss = 0.0;

  public:
  Host() {
    // Null host - should not call!
  }
  
  Host(Pinger new_pinger, Adafruit_NeoPixel * stripPtr, int new_id, String new_name, IPAddress new_ip) {
    myPinger = new_pinger;
    theStrip = stripPtr;
    myIP = new_ip;
    myName = new_name;
    myID = new_id;
  }

  Host(Pinger new_pinger, Adafruit_NeoPixel * stripPtr, int new_id, String new_name, String new_hostname) {
    myPinger = new_pinger;
    theStrip = stripPtr;
    myHostname = new_hostname;
    myName = new_name;
    myID = new_id;
  }

  void ping() {
    TotalSentRequests = 0;
    TotalReceivedResponses = 0;
    AvgResponseTime = 0.0;
    PingLoss = 100.0;
    if( myIP ) {
      myPinger.Ping(myIP);
    } else if( myHostname ) {
      myPinger.Ping(myHostname);
    } else {
      Serial.println("ERR: Tried to ping host, but no IP or hostname configured.");
    }
  }

  String toString() {
    if( myIP ) {
      return myIP.toString();
    } else if( myHostname ) {
       return myHostname;
    } else {
      String ret = "NO hostname configured";
      return ret;
    }
  }

  void render() {
    Serial.println("rendering a host -- ");
    Serial.printf("%s got %lu responses with %.2f%% loss where Average = %.2fms\n", myName.c_str(), TotalReceivedResponses, PingLoss, AvgResponseTime);
    theStrip->setPixelColor(myID, theStrip->Color(0,0,0));
    theStrip->show();
    delay(75);
    
    if( TotalReceivedResponses > 0 ) {
      uint32_t color = theStrip->Color(0, 127, 0);
      theStrip->setPixelColor(myID, color);
    } else {
      theStrip->setPixelColor(myID, theStrip->Color(127, 0, 0));    
    }
    theStrip->show();
  }

  boolean isOnline() {
    return TotalReceivedResponses > 0;
  }
};


// ** Way more fidelity than you need for an LED
bool g_light_on = false;

void light_off() {
  digitalWrite(BUILTIN_LED, HIGH);    // yeah, it's backwards on the ESP
  g_light_on = false;
}
void light_on() {
  digitalWrite(BUILTIN_LED, LOW);    // yeah, it's backwards on the ESP
  g_light_on = true;
}

// ** Flip the LED state ******************************************
void toggle_light() {
  if( g_light_on ) {
    light_off();
  } else {
    light_on();
  }
}





// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

#define HOST_COUNT 6
#define PING_DELAY 10000

Host * currHost;
int currHostID;
Host hosts[HOST_COUNT];

// Set global to avoid object removing after setup() routine
Pinger pinger;


// ** Setup pinger - these are confusing to me *********************************
void configure_pinger_callbacks() {
  pinger.OnReceive([](const PingerResponse& response)
  {
    if (response.ReceivedResponse)
    {
      Serial.printf(
        "Reply from %s: bytes=%d time=%lums TTL=%d\n",
        response.DestIPAddress.toString().c_str(),
        response.EchoMessageSize - sizeof(struct icmp_echo_hdr),
        response.ResponseTime,
        response.TimeToLive);
    }
    else
    {
      Serial.printf("Request timed out.\n");
    }

    // Return true to continue the ping sequence.
    // If current event returns false, the ping sequence is interrupted.
    return true;
  });
  
  pinger.OnEnd([](const PingerResponse& response)
  {
    // Evaluate lost packet percentage
    float loss = 100;
    if(response.TotalReceivedResponses > 0)
    {
      loss = (response.TotalSentRequests - response.TotalReceivedResponses) * 100 / response.TotalSentRequests;
    }
    
    // Print packet trip data
    Serial.printf(
      "Ping statistics for %s:\n",
      response.DestIPAddress.toString().c_str());
    Serial.printf(
      "    Packets: Sent = %lu, Received = %lu, Lost = %lu (%.2f%% loss),\n",
      response.TotalSentRequests,
      response.TotalReceivedResponses,
      response.TotalSentRequests - response.TotalReceivedResponses,
      loss);

    // Print time information
    if(response.TotalReceivedResponses > 0)
    {
      Serial.printf("Approximate round trip times in milli-seconds:\n");
      Serial.printf(
        "    Minimum = %lums, Maximum = %lums, Average = %.2fms\n",
        response.MinResponseTime,
        response.MaxResponseTime,
        response.AvgResponseTime);
    }
    
    // Print host data
    Serial.printf("Destination host data:\n");
    Serial.printf(
      "    IP address: %s\n",
      response.DestIPAddress.toString().c_str());
    if(response.DestMacAddress != nullptr)
    {
      Serial.printf(
        "    MAC address: " MACSTR "\n",
        MAC2STR(response.DestMacAddress->addr));
    }
    if(response.DestHostname != "")
    {
      Serial.printf(
        "    DNS name: %s\n",
        response.DestHostname.c_str());
    }

    // ** hacked in host ping system
    currHost->TotalSentRequests = response.TotalSentRequests;
    currHost->TotalReceivedResponses = response.TotalReceivedResponses;
    currHost->AvgResponseTime = response.AvgResponseTime;
    currHost->PingLoss = loss;

    return true;
  });
}


// ** Setup and connect to wifi network ***************************************
void connect_to_wifi() {
  // Connect to WiFi access point
  bool stationConnected = WiFi.begin("WhiteHouse4", "GUnewhome#1");

  // Check if connection errors
  if(!stationConnected)
    {    Serial.println("Error, unable to connect specified WiFi network.");  }
  
  Serial.print("Connecting to AP...");
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    toggle_light();
    Serial.print(".");
  }
  Serial.print("Ok\n");
}




// ** Setup *********************************************************
void setup()
{  
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  
  strip.begin();            // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();             // Turn OFF all pixels ASAP
  strip.setBrightness(30);  // Set BRIGHTNESS to about 1/5 (max = 255)
  
  connect_to_wifi();
  configure_pinger_callbacks();

  // Configure hosts to ping / test
  hosts[0] = Host(pinger, &strip, 0, "Gateway", WiFi.gatewayIP());
  hosts[1] = Host(pinger, &strip, 1, "Google DNS", IPAddress(8,8,8,8));
  hosts[2] = Host(pinger, &strip, 2, "Open DNS", IPAddress(1,1,1,1));
  hosts[3] = Host(pinger, &strip, 3, "Telstra", "ns1.telstra.net");
  hosts[4] = Host(pinger, &strip, 4, "Europe", "bawk.uk.to");
  hosts[5] = Host(pinger, &strip, 5, "Ada @ GU", "ada.gonzaga.edu");

  Serial.println("Setup complete - wifi ready, start main loop.");
  light_off();              // Turn the LED off since it's bright
}


// ** Main loop *************************************************************************
void loop()
{
  for( int hostID = 0; hostID < HOST_COUNT; hostID++ ) {
    Serial.println("******************************************");
    Serial.println(hosts[hostID].myName);
    currHost = &hosts[hostID];             // Global callback currHost;
    hosts[hostID].ping();
    delay(PING_DELAY);
    hosts[hostID].render();
  }

  // handle light #6 - All good?
  boolean allOnline = true;
  for( int hostID = 0; hostID < HOST_COUNT; hostID++ ) {
    allOnline = allOnline && hosts[hostID].isOnline();
  }
  
  if( allOnline ) {
      uint32_t color = strip.Color(0, 127, 0);
      strip.setPixelColor(6, color);
  } else {
      uint32_t color = strip.Color(127, 0, 0);
      strip.setPixelColor(6, color);
  }
  strip.show();

  
}
