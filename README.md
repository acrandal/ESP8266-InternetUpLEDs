# Arduino / ESP8266 Internet Up LEDs

A small project to light up LEDs to show if hosts are up. 
Uses the ESP8266-Ping library

### Main feature:

Can set up to 6 hosts by IP address or hostname to ping. Each one is ping'ed four times every minute. The results are shown on the lights. The final light goes red if any of the hosts are offline.

### Hardware:
* LOLIN D1 mini - ESP8266 board
* LOLIN RGB LED shield for LOLIN (WEMOS) D1 mini - 7x WS2812B (NeoPixels)

https://docs.wemos.cc/en/latest/d1/d1_mini.html  
https://docs.wemos.cc/en/latest/d1_mini_shiled/rgb_led.html  


### Future work:

* Could be changed to send a text if hosts go down pretty easily or a MQTT message for ITTT rules.
* Change the lights to give more resolution based on the average ping time instead of just a mild green.
* By all means, use a bigger strip of lights and ping *lots* of hosts, though the need to wait for ping callbacks might make it very slow.

### Bugs:

I'm totally sure there's more than a few, but I haven't tested all of the various possible ping timeouts and weird results that could happen.  
Also, this isn't the best code I've ever written (by a long shot). It's pretty hacked together between the Ping library and the need to keep the Host information together. Callbacks are always fun to work with


Author: Aaron S. Crandall <<acrandal@gmail.com>>, 2020  
Version: 1.0  
Copyright: MIT license (except the Adafruit example code - not sure there)  
